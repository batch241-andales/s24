// MINI-ACTIVITY
/*
1. Using the ES6 update, get the cube of 8.
2. Print the result of the console with the message: `The interest rate on your savings account is` + result
3. Use the template literal.
*/

const cube8 = 8 ** 3;
console.log(cube8);

console.log(`The interest rate on your savings account is ${cube8}`);

// MINI-ACTIVITY
const address = ["258", "Washinton Ave NW", "California", "99011"];

const [number, street, state, postal] = address;
console.log(`I live at ${number}, ${street}, ${state}, ${postal}`);


const animal = {
	name: "Lolong",
	species: "Saltwater Crocodile",
	weight: "1075 kgs",
	measurement: "20ft 3 in"
}

const {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weight at ${weight} with a measurement of ${measurement}`);

let numbers = [1,2,3,4,5]

numbers.forEach((number) => console.log(number));
function sum(total, num){
	return total + num
}
let reducednumber = numbers.reduce((num1, num2) => num1+num2)
console.log("Reduced Number");
console.log(reducednumber);


class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog("Biste", 4, "Aspin");
console.log("")
console.log(myDog)