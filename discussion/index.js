//console.log("Hello");

// ES6 Updates


// EXPONENT OPERATOR

//old
console.log("Result of old: ")
const oldNum = Math.pow(8,2)
console.log(oldNum);

//new
console.log("Result of ES6: ")
const newNum = 8 ** 2;
console.log(newNum);

// TEMPLATE LITERALS
/*
- Allows us to write strings without using the concatenate operator (+)
*/

let studentName = "Roland";

// Pre-template Literal String
console.log("Pre-template literal")
console.log("Hello " + studentName + "! Welcome to programming");

// Template Literal String
console.log("Template literal")
console.log(`Hello ${studentName}! Welcome to programming!`)

// Multi-line template literal
const message  = `
${studentName} attended a math competion.
He won it by solving the problem 8 ** 2 with the solution of
${newNum}.
`
console.log(message);
/*
- Template literals allow us to write strings with embedded javascript experessions
- "${}" are used to include Javascript expression in strings using template literals
*/

const interestRate = .1;
const principal = 1000;
console.log(`The interest rate on your savings account is: ${principal * interestRate} `);

// ARRAY DESTRUCTURING
/*
- Allows us to unpack elements in arrays into distinct variables
- Allows us to name array elements with variables instead of using index numbers
- Syntax:
	let / const [variableName, variableName, variableName] = array;
*/
console.log("Array Destructuring: ")
const fullName = ["Jeru" , "Nebur", "Palma"];
// Pre-Array Destructuring
console.log("Pre-Array Destructuring: ")
console.log(fullName[0])
console.log(fullName[1])
console.log(fullName[2])
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}`)


// Array Destructuring
console.log("Array Destructuring:")
const [firstName, middleName, lastName] = fullName;
console.log(firstName)
console.log(middleName)
console.log(lastName)
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`)

// OBJECT DESTRUCTURING
/*
- Allows us ti unpack properties on objects into distinct values
- Shortens the syntax for accessing properties from objects
- Syntax:
	let / const { propertyName, propertyName, propertyName} = object;
*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)
console.log(`Hello ${person.givenName} ${person.maidenName}. ${person.familyName}`)

// Object Destructuring
const {givenName, maidenName, familyName } = person;
console.log(givenName)
console.log(maidenName)
console.log(familyName)
console.log(`Hello ${givenName} ${maidenName}. ${familyName}`)

// ARROW FUNCTIONS
/*
- Compact alternative syntax to traditional functions
- Useful for creating code snippets where creating functions will not be reused in any other portions of the code
- Adheres to the DRY principles (don't Repeat Yourself) where there is no longer a need to create a new function and think of a name for function that will only be used in certain code snippets
*/

const hello = () => {
	return "Good morning Batch 241"
}
hello();

/*
Syntax:
	let / const variableName = (parameterA, parameterB, parameterC) => {
		console.log()
	}
*/

const printFullName = (firstN, middleI, lastN) => {
	console.log(`${firstN} ${middleI}. ${lastN}`)
}
printFullName(`John`, `D`, `Smith`)
printFullName(`Eric`, `B`, `Andales`)

const students = ["John" , "Jane", "Smith"]

students.forEach(function (student){
	console.log(`${student} is a student`)
})

// Arrow function with loops

students.forEach((student) => {
	console.log(`${student} is a student`)
})

// IMPLICIT RETURN STATEMENT
/*
- There are instances when you can omit the "return" statement
- This works because even without the return statament, Javascript adds for it for the result of the function
*/

// Old / Pre-Arrow function
const add = (x,y) => {
	return x + y
}
let total = add(1,2)
console.log(total);

// Arrow function
const subtract = (x,y) => x - y 
let difference = subtract(3,1)
console.log(difference)


// DEFAULT ARGUMENT VALUE
// Provides default argument if none is provided when the function is invoked
const greet = (name = 'User') => {
	return `Goodmorning, ${name}`
}
console.log(greet())
console.log(greet("Eric"))

// CLASS-BASED OBJECT BLUEPRINTS
// Allows creation / instantiation of objects as blueprints

// Creating a class
/*
- The constructor function is a special method of class for creating / initializing an object for that class.
- The "this" keyowrd refers to the properties initialized from the inside the class
- Syntax:
	class className{
		constructor(objectPropertyA, objectPropertyB){
			this.objectPropertyA = objectPropertyA
			this.objectPropertyB = objectPropertyB
		}
	}
*/

class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}
/*
- The "new" operator creates / instantiates a new object with the given arguments as the value
- No arguments provided will create an object without any values assigned to its properties
- Syntax:
	let / const variableName = new className()
*/

console.log("")
console.log("Class Blueprints")
let myCar = new Car();
console.log(myCar)

console.log("console");
console.log(console);
console.log(window)

// Assigning properties after creation / instantiation of an object
myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);

// Instantiate a new object
const myNewCar = new Car("Toyota", "Vios", 2021)
console.log(myNewCar);